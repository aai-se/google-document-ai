/*
 * Copyright (c) 2021 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.google.documentai;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.SelectModes;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.automationanywhere.googlecloud.GoogleAuth;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Start Session", name = "startGoogleDocumentAISession", description = "Start new session", icon = "pkg.svg", background_color = "#EDEDED" , comment = true ,node_label = "start session {{sessionName}}|"  ) 
public class StartDocumentAISession{
 
    @Sessions
    private Map<String, Object> sessions;
    
    
    
	private GoogleSettings settings;

    
    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "DocumentAI") @NotEmpty String sessionName,
		              @Idx(index = "2", type = AttributeType.SELECT, options = {
				       @Idx.Option(index = "2.1", pkg = @Pkg(label = "Auth File", value = "FILE")), 
				       @Idx.Option(index = "2.2", pkg = @Pkg(label = "Auth Parameter", value = "PARA"))})
			           @Pkg(label = "Authentication", description = "", default_value = "FILE", default_value_type = STRING)
					   @SelectModes  @NotEmpty String authtype,
					   @Idx(index = "2.1.1", type = AttributeType.FILE)  @Pkg(label = "Google API Authentication File (JSON)" , default_value_type = DataType.FILE) @NotEmpty  String jsonPath,
		          	   @Idx(index = "2.1.2", type = CREDENTIAL) @Pkg(label = "Project ID",  default_value_type = STRING) @NotEmpty SecureString projectID,
		               
		          	   @Idx(index = "2.2.1", type = CREDENTIAL) @Pkg(label = "Auth Project ID",  default_value_type = STRING) @NotEmpty SecureString authprojectID,
		               @Idx(index = "2.2.2", type = CREDENTIAL) @Pkg(label = "Resource Project ID",  default_value_type = STRING)  SecureString resourceprojectID,
		               @Idx(index = "2.2.3", type = CREDENTIAL) @Pkg(label = "Private Key ID",  default_value_type = STRING) @NotEmpty SecureString private_key_id,
		               @Idx(index = "2.2.4", type = CREDENTIAL) @Pkg(label = "Private Key",  default_value_type = STRING) @NotEmpty SecureString private_key,
		               @Idx(index = "2.2.5", type = CREDENTIAL) @Pkg(label = "Client Email",  default_value_type = STRING) @NotEmpty SecureString client_email,
		               @Idx(index = "2.2.6", type = CREDENTIAL) @Pkg(label = "Client ID",  default_value_type = STRING) @NotEmpty SecureString client_id,	
   				   	  @Idx(index = "3", type = AttributeType.RADIO, options = {
								@Idx.Option(index = "3.1", pkg = @Pkg(label = "US", value = "us")),
								@Idx.Option(index = "3.2", pkg = @Pkg(label = "EU", value = "eu"))
   				   			}) @Pkg(label = "Location", default_value = "us", default_value_type = STRING) @NotEmpty String location ) throws Exception {
 
        // Check for existing session
        if (sessions.containsKey(sessionName))
            throw new BotCommandException("Session name in use") ;
       
        String resourceID;
         if (authtype.equals("PARA")) {
            resourceID = (resourceprojectID.getInsecureString().isEmpty()) ? authprojectID.getInsecureString() : resourceprojectID.getInsecureString() ;
            jsonPath = new GoogleAuth().writeAutFile(authprojectID.getInsecureString(), private_key_id.getInsecureString(), private_key.getInsecureString(), client_email.getInsecureString(), client_id.getInsecureString());
        }
        else {
        	resourceID = projectID.getInsecureString();
        }
        settings = new GoogleSettings();
        
        settings.JsonAuthFile = jsonPath;
        settings.location = location;
        settings.projectID = resourceID;
        this.sessions.put(sessionName, this.settings);
    }
 
    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    

    
 
    
    
}