/*
 * Copyright (c) 2021 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.google.documentai;



import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.data.impl.NumberValue;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.googlecloud.GoogleDocumentAIV3;
import com.automationanywhere.googlecloud.GoogleTable;
import com.automationanywhere.commandsdk.annotations.Execute;





/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Google Document Splitter Sync", name = "SyncGoogleDocumentSplit",
        description = "Split Document with a Sync Call",
        node_label = "Google Document Splitter(Sync)", icon = "pkg.svg",  comment = true ,background_color = "#EDEDED" ,   
        return_type=DataType.DICTIONARY,  return_sub_type=DataType.ANY , return_label="Dictionary with keys 'entities' (JSON) , 'content (String)", return_required=true)

public class GoogleSplitterSync{
	
    @Sessions
    private Map<String, Object> sessions;
	   
    
	private static final Logger logger = LogManager.getLogger(GoogleSplitterSync.class);
	@Execute
    public   DictionaryValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING, default_value = "DocumentAI") @NotEmpty String sessionName ,
	          					     @Idx(index = "2", type = AttributeType.FILE )  @Pkg(label = "Input File Path" , default_value_type = DataType.FILE )  @NotEmpty  @FileExtension("pdf,tiff,gif") String file,
		            		         @Idx(index = "3", type = TEXT) @Pkg(label = "Parser ID",  default_value_type = STRING ) @NotEmpty String parserID
        			   	  ) throws Exception
     {
		
		
		logger.info("Start GoogleDocumentSplitterSync ");
		LinkedHashMap<String, Value> resultMap = new LinkedHashMap<String,Value>();
		DictionaryValue result = new DictionaryValue();
		GoogleSettings settings = (GoogleSettings) this.sessions.get(sessionName);  
		
		
		logger.info("JSON Auth "+settings.JsonAuthFile);
		GoogleDocumentAIV3 processor = new GoogleDocumentAIV3();
		
		
		processor.processDocumentwithSplitterSync( settings.projectID, settings.location, parserID, file,settings.JsonAuthFile);

		String content = processor.getFullText();
		String entities = processor.getEntities();
		
		resultMap.put("content", new StringValue(content));
		
		resultMap.put("entities", new StringValue(entities));
		
		result.set(resultMap);
		return result;

     
     }
	
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
		
	
}