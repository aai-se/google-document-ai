/*
 * Copyright (c) 2021 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.google.documentai;



import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.data.impl.NumberValue;

import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.FileExtension;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.googlecloud.GoogleDocumentAIV3;
import com.automationanywhere.googlecloud.GoogleTable;
import com.automationanywhere.commandsdk.annotations.Execute;





/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Google Form Parser Sync", name = "SyncGoogleFormParserSync",
        description = "Parse Form with a Sync Call",
        node_label = "Google Form Parser (Sync)", icon = "pkg.svg",  comment = true ,background_color = "#EDEDED" ,   
        return_type=DataType.DICTIONARY,  return_sub_type=DataType.STRING , return_label="Dictionary with keys 'fields' (Dictionary), 'tables' (List of Table) , 'output' (String), 'pagecount' (Number) , 'languages' (List of String) , 'entities' (JSON) , 'content (String)", return_required=true)

public class GoogleFormParserSync{
	
    @Sessions
    private Map<String, Object> sessions;
	   
    
	private static final Logger logger = LogManager.getLogger(GoogleFormParserSync.class);
	@Execute
    public   DictionaryValue  action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING, default_value = "DocumentAI") @NotEmpty String sessionName ,
	          					  @Idx(index = "2", type = AttributeType.FILE )  @Pkg(label = "Input File Path" , default_value_type = DataType.FILE )  @NotEmpty  @FileExtension("pdf,tiff,gif") String file,
	          					  @Idx(index = "3", type = AttributeType.TEXT)  @Pkg(label = "Output Dir Path" , default_value_type = DataType.STRING, description = "Directory where PDF with Annotations in the format <Original File Name>_annotation.pdf will be stored")  String outputPath,
	            		          @Idx(index = "4", type = TEXT) @Pkg(label = "Parser ID",  default_value_type = STRING ) @NotEmpty String parserID,
	            		          @Idx(index = "5", type = AttributeType.BOOLEAN) @Pkg(label = "Include Header in 1. table row",  default_value_type = DataType.BOOLEAN, default_value="false") @NotEmpty Boolean includeheader

        			   	  ) throws Exception
     {
		
		
		logger.info("Start GoogleFormParserSync ");
		LinkedHashMap<String, String> fieldMap = new LinkedHashMap<String,String>();
		LinkedHashMap<String, Value> resultMap = new LinkedHashMap<String,Value>();
		DictionaryValue result = new DictionaryValue();
		GoogleSettings settings = (GoogleSettings) this.sessions.get(sessionName);  
		
		
		logger.info("JSON Auth "+settings.JsonAuthFile);
		GoogleDocumentAIV3 processor = new GoogleDocumentAIV3();
		
		
		fieldMap = processor.processDocumentwithProcessorSync( settings.projectID, settings.location, parserID, file,settings.JsonAuthFile,outputPath,includeheader);
		String outputfile = processor.getOutputFile();
		Integer noofpages = processor.getpageCount();
		List<String> languages = processor.getLanguage() ;
		List<GoogleTable> tables = processor.getTables();
		String content = processor.getFullText();
		String entities = processor.getEntities();
		
		List<TableValue> resultTables = new ArrayList<TableValue>();
		for (Iterator iterator = tables.iterator(); iterator.hasNext();) {
			GoogleTable gtable= (GoogleTable) iterator.next();
			Table returnTable = new Table();
			List<Schema> tableSchema = new ArrayList<Schema>();
			for (int i = 0; i < gtable.columnNames.length; i++) {
				Schema schema = new Schema();
				schema.setName(gtable.columnNames[i]);
	//			schema.setType(com.automationanywhere.botcore.api.dto.AttributeType.STRING);
				tableSchema.add(schema);
			}
			returnTable.setSchema(tableSchema);
			
			ArrayList<Row> returnrows = new ArrayList<Row>();
			for (String[] row : gtable.data)  {
				List<Value> rowvalues = new ArrayList<Value>();
				for (String cell : row) {
		            rowvalues.add(new StringValue(cell));
				}
				Row returnrow = new Row();
				returnrow.setValues(rowvalues);
				returnrows.add(returnrow);
			}
		    returnTable.setRows(returnrows);
				
			TableValue returnTableValue = new TableValue();
			returnTableValue.set(returnTable);
			resultTables.add(returnTableValue);
			
		}
		
		ListValue listTables = new ListValue<>();
		listTables.set(resultTables);
		resultMap.put("tables", listTables);
		
		
		LinkedHashMap<String, Value> resultfieldMap = new LinkedHashMap<String, Value>();
		for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
			resultfieldMap.put(entry.getKey(), new StringValue(entry.getValue()));
		}
		DictionaryValue resultfields = new DictionaryValue();
		resultfields.set(resultfieldMap);
		resultMap.put("fields", resultfields);
		
		resultMap.put("output", new StringValue(outputfile));
		
		resultMap.put("pagecount", new NumberValue(noofpages));
		
		resultMap.put("content", new StringValue(content));
		
		
		
		
		List<Value> resultlang = new ArrayList<Value>();
		for (Iterator iterator = languages.iterator(); iterator.hasNext();) {
			String lang = (String) iterator.next();
			resultlang.add(new StringValue(lang));
		}
		
		ListValue<String> resultlangvalue = new ListValue<String>();
		resultlangvalue.set(resultlang);
		
		resultMap.put("languages", resultlangvalue);
		
		resultMap.put("entities", new StringValue(entities));
		
		result.set(resultMap);
		return result;

     
     }
	
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
	
		
	
}