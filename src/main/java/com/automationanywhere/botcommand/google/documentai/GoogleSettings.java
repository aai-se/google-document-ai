
/*
 * Copyright (c) 2021 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.google.documentai;

public class GoogleSettings {
	
	public String JsonAuthFile;
	public String projectID;
	public  String location;

}
