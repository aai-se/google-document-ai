package com.automationanywhere.googlecloud;

public class GoogleTable {
	public String[] columnNames ;
	public String[][] data;
	
	
	public void printTable() {
		for (int i = 0; i < columnNames.length; i++) {
			System.out.print(columnNames[i]);
			System.out.print("\t");
		}
	//	System.out.println();
		
		 for(String[] row : data) {
				for (String cell : row) {
		            System.out.print(cell);
		            System.out.print("\t");
	        }

		}
	//	System.out.println();
	
	}
	
	
}
