package com.automationanywhere.googlecloud;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automationanywhere.botcommand.exception.BotCommandException;

/**
 * The Class AESEncrypter. It implements AES algorithm for secure 2-tier
 * authentication. https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
 */
public class AESEncrypter {

    /** The cipher used for encryption. */
    private static Cipher cipherForEncryption;

    /** The cipher used for decryption. */
    private static Cipher cipherForDecryption;

    /** The secret key. A shared secret between the sender and recipient */
    private static SecretKey secretKey;

    /** The logger. */
    private static final Logger LOGGER = LogManager.getLogger(AESEncrypter.class);

    static {

        secretKey = new SecretKeySpec("AAOffice365AuthSecretKey".getBytes(StandardCharsets.UTF_8), "AES");
        try {
            cipherForDecryption = Cipher.getInstance("AES");
            cipherForDecryption.init(Cipher.DECRYPT_MODE, secretKey);

            cipherForEncryption = Cipher.getInstance("AES");
            cipherForEncryption.init(Cipher.ENCRYPT_MODE, secretKey);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException exception) {
            LOGGER.error("Exception when performing encryption on data...", exception);
            
        }
    }

    /**
     * Instantiates a new AES encrypter.
     */
    private AESEncrypter() {

    }

    /**
     * Decrypts the text.
     *
     * @param encryptedText the encrypted text
     * @return the string
     * @throws UnsupportedEncodingException
     * @throws Exception                    the exception
     */
    public static String decrypt(String encryptedText) throws Exception {

            byte[] encryptedTextByte = Base64.decodeBase64((encryptedText.getBytes(StandardCharsets.UTF_8)));
            byte[] decryptedByte;
            decryptedByte = cipherForDecryption.doFinal(encryptedTextByte);
            return new String(decryptedByte, "UTF-8");

    }

    /**
     * Encrypts the plain text.
     *
     * @param plainText the plain text
     * @return the string
     * @throws Exception 
     * @throws Exception the exception
     */
    public static String encrypt(String plainText) throws Exception {

            byte[] plainTextByte = plainText.getBytes(StandardCharsets.UTF_8);
            byte[] encryptedByte;
            encryptedByte = cipherForEncryption.doFinal(plainTextByte);

            String encryptedText = Base64.encodeBase64URLSafeString(encryptedByte);
            return URLEncoder.encode(encryptedText, "UTF-8");


    }

}

