package com.automationanywhere.googlecloud;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.documentai.v1beta3.Document;
import com.google.cloud.documentai.v1beta3.Document.Entity;
import com.google.cloud.documentai.v1beta3.Document.EntityRelation;
import com.google.cloud.documentai.v1beta3.Document.Page;
import com.google.cloud.documentai.v1beta3.Document.Page.Block;
import com.google.cloud.documentai.v1beta3.Document.Page.DetectedLanguage;
import com.google.cloud.documentai.v1beta3.Document.Page.FormField;
import com.google.cloud.documentai.v1beta3.Document.Page.Layout;
import com.google.cloud.documentai.v1beta3.Document.Page.Table;
import com.google.cloud.documentai.v1beta3.Document.Page.Table.TableCell;
import com.google.cloud.documentai.v1beta3.Document.Page.Table.TableRow;
import com.google.cloud.documentai.v1beta3.Document.PageAnchor;
import com.google.cloud.documentai.v1beta3.Document.PageAnchor.PageRef;
import com.google.cloud.documentai.v1beta3.Document.TextAnchor;
import com.google.cloud.documentai.v1beta3.Document.TextAnchor.TextSegment;
import com.google.cloud.documentai.v1beta3.DocumentProcessorServiceClient;
import com.google.cloud.documentai.v1beta3.DocumentProcessorServiceSettings;
import com.google.cloud.documentai.v1beta3.NormalizedVertex;
import com.google.cloud.documentai.v1beta3.ProcessRequest;
import com.google.cloud.documentai.v1beta3.ProcessResponse;
import com.google.cloud.documentai.v1beta3.ProcessorName;
import com.google.cloud.documentai.v1beta3.RawDocument;
import com.google.protobuf.ByteString;


import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONObject;

public class GoogleDocumentAIV3{

	private Integer NoPages;
	private String OutputFile;

	private List<String> mostconfidentlanguage;
	private List<GoogleTable> tables;
	private String entities;
	private String text;
	
	
  public  LinkedHashMap<String,String> processDocumentwithProcessorSync (
      String projectId, String location, String processorId, String filePath,String jsonPath, String outputPath,Boolean includeHeader)
      throws Exception {

	  LinkedHashMap<String,String> fieldsMap = new LinkedHashMap<String,String>();
	  
	  DocumentProcessorServiceClient client;
	  if (jsonPath != null) {
			 Collection<String> scopes = new ArrayList<String>();
			 scopes.add("https://www.googleapis.com/auth/cloud-platform");
		  GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath)).createScoped(scopes);
	      DocumentProcessorServiceSettings settings = DocumentProcessorServiceSettings.newBuilder()
	    		    .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
	    		    .build();
	      client = DocumentProcessorServiceClient.create(settings);
	  }
	  else {
		  
	      client = DocumentProcessorServiceClient.create();
	  }
	  
	  

    
      String name = ProcessorName.format(projectId, location, processorId);

      // Read the file.
      byte[] imageFileData = Files.readAllBytes(Paths.get(filePath));

      // Convert the image data to a Buffer and base64 encode it.
      ByteString content = ByteString.copyFrom(imageFileData);

      Optional<String> extension = getExtensionByStringHandling(filePath);
      
      RawDocument document;
      String mimetype ;
   	  String ext = extension.get().toLowerCase();
      if (ext != "")
      {
    	  switch(ext) {
    	  	case "tiff":  
    	  	case "tif":
    	  			mimetype ="image/tiff";
    	  			break;
    	  	case "gif":
	  				mimetype ="image/tif";
	  				break;
    	  	case "pdf":
	  				mimetype ="application/pdf";
	  				break;
	  		default:
	      	  throw new Exception("File Type "+ext+" not supported");
    	  }
    	  document = RawDocument.newBuilder().setContent(content).setMimeType(mimetype).build();
      }
      else {
    	  throw new Exception("File Type "+ext+" not supported");
      }
    	  


	// Configure the process request.
      ProcessRequest request =
          ProcessRequest.newBuilder().setName(name).setRawDocument(document).build();
      // Recognizes text entities in the PDF document
      ProcessResponse result = client.processDocument(request);
      Document documentResponse = result.getDocument();


      
      // Get all of the document text as one big string
      this.text = documentResponse.getText();
      this.NoPages = documentResponse.getPagesCount();

      // Read the text recognition output from the processor
      List<Page> pages = documentResponse.getPagesList();
      List<String> outImages = new ArrayList<String>();
      tables = new ArrayList<GoogleTable>();
      this.mostconfidentlanguage = new ArrayList<String>();
      for (Iterator<Page> iterator = pages.iterator(); iterator.hasNext();) {
    	  Page page = (Page) iterator.next();
    	  List<Table> pagetables = page.getTablesList();
    	  
    	  
    	 for (Iterator iterator2 = pagetables.iterator(); iterator2.hasNext();) {
    		 Table table = (Table) iterator2.next();
    		 if (hasHeaders(table, text)) {
    			 GoogleTable gTable = new GoogleTable();
    			 gTable.columnNames = new String[table.getHeaderRows(0).getCellsCount()];

    			 List<TableRow> headers = table.getHeaderRowsList();
    			 for (int i = 0; i < headers.size(); i++) {
    				 TableRow tableRow = headers.get(i);
    				 List<TableCell> rowcells = tableRow.getCellsList();
    				 for (int j = 0; j < rowcells.size(); j++) {
    					 gTable.columnNames[j] = (gTable.columnNames[j] == null) ? "" : gTable.columnNames[j];
    					 TableCell tableCell = rowcells.get(j);
    					 gTable.columnNames[j] = gTable.columnNames[j] + " "+ getText(tableCell.getLayout(), text);
    				 }
    			 }
		
    			 List<TableRow> rows = table.getBodyRowsList();
    			 int startIndex =0;
    			 if (includeHeader) {
        			 gTable.data = new String[rows.size()+1][maxColumns(table)];
    				 startIndex =1;
    				 gTable.data[0] =  gTable.columnNames ;
    			 }
    			 else
    			 {
        			 gTable.data = new String[rows.size()][maxColumns(table)];
    				 startIndex =0;
    			 }
    			 for (int i = 0; i < rows.size(); i++) {
    				 TableRow tableRow = rows.get(i);
    				 List<TableCell> rowcells = tableRow.getCellsList();
    				 for (int j = 0; j < rowcells.size(); j++) {
    					 TableCell tableCell = rowcells.get(j);
    					  gTable.data[i+startIndex][j] = getText(tableCell.getLayout(), text);

    				 }
    			 }
    			 this.tables.add(gTable);
    		 }
    	 }

    	 JSONObject entityObject = new JSONObject();
         List<Entity> entitiesResponse = documentResponse.getEntitiesList();
         List<EntityRelation> entitiesRelations = documentResponse.getEntityRelationsList();
         for (Iterator iterator2 = entitiesRelations.iterator(); iterator2.hasNext();) {
			EntityRelation entityRelation = (EntityRelation) iterator2.next();
		}
         
        
         for (Iterator iterator3 = entitiesResponse.iterator(); iterator3.hasNext();) {
       	  	Entity entity = (Entity) iterator3.next();
       	  	System.out.print(entity.toString());
    	  	getEntityProps(entity,entityObject,false);	
         }
         entities = entityObject.toString();

    	  
    	  List<DetectedLanguage> detectedlanguages = page.getDetectedLanguagesList();
    	  String language = "";
    	  float confidence = 0.0f;
    	  for (Iterator iterator2 = detectedlanguages.iterator(); iterator2.hasNext();) {
			DetectedLanguage detectedLanguage = (DetectedLanguage) iterator2.next();
			if (detectedLanguage.getConfidence() > confidence) {

				confidence = detectedLanguage.getConfidence();
				language = detectedLanguage.getLanguageCode();
			}
		  }
    	  if (detectedlanguages.size() > 0) {
    		  mostconfidentlanguage.add(detectedlanguages.get(0).getLanguageCode());
    	  }
    	  else {
    		  mostconfidentlanguage.add("unknown");
    	  }

    	  
    	  
    	  if (outputPath != "") {
    		  outImages.add(drawBoxes(page,filePath,outputPath));
    	  }


    	  for (Document.Page.FormField field : page.getFormFieldsList()) {
    		  String fieldName = getText(field.getFieldName(), text);
    		  String fieldValue = getText(field.getFieldValue(), text);
    		  fieldsMap.put(fieldName, fieldValue);
    	  }
     }
      
      String[] outImagesArray =  new String[outImages.size()];
      outImagesArray= outImages.toArray(outImagesArray);
      if (outputPath != "") {
    	this.OutputFile = outputPath+"/"+getFileName(filePath)+"_annotation.pdf";
		PDFUtils.combineImagesIntoPDF(this.OutputFile, outImagesArray); 
      }

    return fieldsMap;
  }
  
  
 

  public static LinkedHashMap<String,String> listProcessors(String projectId, String location,String jsonPath) throws IOException {
	  LinkedHashMap<String,String> processorsMap = new LinkedHashMap<String,String>();  
	  DocumentProcessorServiceClient client = DocumentProcessorServiceClient.create();
	  
      return processorsMap;
}


  private  String drawBoxes(Page page,String file,String outPath) throws IOException {

	    byte [] b = new byte[page.getImage().getContent().size()] ;
	  	page.getImage().getContent().copyTo(b, 0);
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        BufferedImage image = ImageIO.read(in);
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setStroke(new BasicStroke(3));
		Font stringFont = new Font("Arial",Font.BOLD,20);
		g.setFont(stringFont);
		List<NormalizedVertex> vertices ;
        List<Block> blocks = page.getBlocksList();
        int w = (int)page.getDimension().getWidth();
        int h = (int)page.getDimension().getHeight();
        for (Iterator iterator = blocks.iterator(); iterator.hasNext();) {
			Block block = (Block) iterator.next();
			Layout layout = block.getLayout();
			vertices = layout.getBoundingPoly().getNormalizedVerticesList();
			drawPoly(g,vertices,Color.ORANGE,block.getDescriptorForType().getName(),w,h,true);
		}

		List<FormField> fields = page.getFormFieldsList();
		for (Iterator iterator = fields.iterator(); iterator.hasNext();) {
			FormField formField = (FormField) iterator.next();
			vertices= formField.getFieldName().getBoundingPoly().getNormalizedVerticesList();
			drawPoly(g,vertices,new Color(0,76,153),"N",w,h,false);
			vertices= formField.getFieldValue().getBoundingPoly().getNormalizedVerticesList();
			drawPoly(g,vertices,new Color(0,153,0),"V",w,h,false);
		}
		
		 List<Table> pagetables = page.getTablesList();
    	 for (Iterator iterator2 = pagetables.iterator(); iterator2.hasNext();) {
	    	Table table = (Table) iterator2.next();
			 if (hasHeaders(table, this.text)) {
	    	  vertices=  table.getLayout().getBoundingPoly().getNormalizedVerticesList();
	    	  drawPoly(g,vertices,new Color(102,0,204),"T",w,h,false);
			 }
    	 }
		g.dispose();

		File outputfile = new File(outPath+"/"+getFileName(file)+"_"+page.getPageNumber()+".png");
		ImageIO.write(image, "png", outputfile);
		return outputfile.getPath();
  }

  
 
  
  
  
  private static void drawPoly(Graphics2D g, List<NormalizedVertex> vertices, Color color, String label,int w,int h,boolean fill) {
	  int[] x = new int[vertices.size()];
	  int[] y = new int[vertices.size()];
	  for (int i = 0; i < vertices.size(); i++) {
		x[i] = (int)(vertices.get(i).getX()*w);
		y[i] = (int)(vertices.get(i).getY()*h);		
	  }
	  if (fill) {
		  g.setComposite(AlphaComposite.SrcOver.derive(0.2f));
          g.setColor(color);
		  g.fillPolygon(x, y,vertices.size());
	  }
	  else
	  {   
		  g.setComposite(AlphaComposite.SrcOver.derive(0.6f));
		  g.setColor(color);
	      g.drawPolygon(x, y,vertices.size());
  	     if (vertices.size() > 1 && label != "") {
	 	   g.drawString(label, x[vertices.size()-2], y[vertices.size()-2]);
  	     }
	  }
 
  }


  private static void getEntityProps(Entity entity,JSONObject entityObject,Boolean splitter) {

	  System.out.println(entity.getPropertiesCount());
 	  	if (entity.getPropertiesCount() > 0) {
   	  		List<Entity> props = entity.getPropertiesList();
   	  		JSONArray propsArray = new JSONArray();
   	  		JSONObject subEntityObject = new JSONObject();
   	  		for (Iterator iterator = props.iterator(); iterator.hasNext();) {
   	  			Entity entityProp = (Entity) iterator.next();
   	  			getEntityProps(entityProp,subEntityObject,splitter);
   	  	
   	  		}
	  		if (entityObject.has(entity.getType()))
	  		{
	  			JSONArray currentArray = entityObject.getJSONArray(entity.getType());
	  			currentArray.put(subEntityObject);
	   	  	    entityObject.put(entity.getType(), currentArray);
	  		}
	  		else
	  		{
		  		propsArray.put(subEntityObject);
	   	  	    entityObject.put(entity.getType(), propsArray);
	  		}
   	  	}
 	  	else {
 	  		if (!splitter) {
 	  			String[] splitDescription = entity.getType().split("/");
 	  			String entityName = (splitDescription.length > 1) ? splitDescription[1] : entity.getType();
 	  			entityObject.put(entityName , entity.getMentionText());
 	  		}
 	  		else {
 	  			entityObject.put("confidence" , entity.getConfidence());
 	  			entityObject.put("type" , entity.getType());
 	  			TextAnchor textAnchor = entity.getTextAnchor();
 	  			JSONObject textanchorObj = new JSONObject();
 	  			List<TextSegment> textSegments = textAnchor.getTextSegmentsList();
 	  			JSONArray segmentsObj = new JSONArray();
 	  			for (Iterator iterator = textSegments.iterator(); iterator.hasNext();) {
					TextSegment textSegment = (TextSegment) iterator.next();
					JSONObject segmentObj = new JSONObject();
					segmentObj.put("start_index",textSegment.getStartIndex());
					segmentObj.put("end_index",textSegment.getEndIndex());
					segmentsObj.put(segmentObj);
 	  			}
 	  			textanchorObj.put("text_segments", segmentsObj);
 	  			entityObject.put("text_anchor", textanchorObj);
 	  			
 	  			PageAnchor pageAnchor = entity.getPageAnchor();
 	  			JSONObject pageanchorObj = new JSONObject();
 	  			List<PageRef> pagerefs = pageAnchor.getPageRefsList();
	  			JSONArray pagerefsObj = new JSONArray();
 	  			for (Iterator iterator = pagerefs.iterator(); iterator.hasNext();) {
					PageRef pageRef = (PageRef) iterator.next();
					pagerefsObj.put(pageRef.getPage());
 	  			}
 	  			pageanchorObj.put("page_refs", pagerefsObj);
 	  			entityObject.put("page_anchor", pageanchorObj);
 	  			

 	  			entityObject.put("type" , entity.getType());
					
			}
 	  	}
 	  		
 	  	
   }
  
  

  private static String getText(Layout layout, String text) {
	if (layout.hasTextAnchor()) {
	  TextAnchor textAnchor = layout.getTextAnchor();
      if (textAnchor.getTextSegmentsList().size() > 0) {
        int startIdx = (int) textAnchor.getTextSegments(0).getStartIndex();
        int endIdx = (int) textAnchor.getTextSegments(0).getEndIndex();
        return text.substring(startIdx, endIdx);
      }
      return "";
	}
	else {
		return "";
	}
	
  }
  
  
  
  
  private static boolean hasHeaders(Table table, String text) {
	  boolean hasHeaderswithContent = false;
	  
	  List<TableRow> headers = table.getHeaderRowsList();
	  for (Iterator iterator3 = headers.iterator(); iterator3.hasNext();) {
		 TableRow tableRow = (TableRow) iterator3.next();
		  List<TableCell> rowcells = tableRow.getCellsList();
			for (Iterator iterator4 = rowcells.iterator(); iterator4.hasNext();) {
				TableCell tableCell = (TableCell) iterator4.next();
				String cellText = getText(tableCell.getLayout(), text);
				hasHeaderswithContent = (cellText =="") ? false : true;
			}
		}
	  
	  return hasHeaderswithContent;
  }
  
  private static int maxColumns(Table table) {
	  int maxcolumns = 0;
	  
	  List<TableRow> rows = table.getBodyRowsList();
	  for (int i = 0; i < rows.size(); i++) {
		 TableRow tableRow = rows.get(i);
		 int cellCount = tableRow.getCellsCount();
		 maxcolumns = (cellCount > maxcolumns) ? cellCount : maxcolumns;
	 }
	  
	  return maxcolumns;
  }

  private static String getFileName(String fullPath) {
  		Path path = Paths.get(fullPath); 
          String fileName = path.getFileName().toString();
          int index = fileName.lastIndexOf('.');
          if (index > 0) {
            fileName = fileName.substring(0, index);
          }
        
          return fileName;  
  }

  public List<String> getLanguage() {
	  return this.mostconfidentlanguage;
  }
  public Integer getpageCount() {
	  return this.NoPages;
  }
  
  public String getOutputFile() {
	  return this.OutputFile;
  }
  
  
  public List<GoogleTable> getTables() {
	  return tables;
  }
  
  
  public String getEntities() {
	  return entities;
  }
  
  public String getFullText() {
	  return this.text;
  }
  
  public Optional<String> getExtensionByStringHandling(String filename) {
	    return Optional.ofNullable(filename)
	      .filter(f -> f.contains("."))
	      .map(f -> f.substring(filename.lastIndexOf(".") + 1));
	}




public  LinkedHashMap<String,String> processDocumentwithProcessorASync_Send (
	      String projectId, String location, String processorId, String filePath,String jsonPath, String outputBucket, String prefix , Boolean includeHeader)
	      throws IOException, InterruptedException, ExecutionException, TimeoutException {

		  LinkedHashMap<String,String> fieldsMap = new LinkedHashMap<String,String>();
/*		  
		  GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath))
			        .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
		  DocumentProcessorServiceSettings settings  =
				     DocumentProcessorServiceSettings.newBuilder()
				         .setCredentialsProvider( FixedCredentialsProvider.create(ServiceAccountCredentials.fromStream(new FileInputStream(jsonPath))))
				         .build();
		  
		  DocumentProcessorServiceClient client = DocumentProcessorServiceClient.create();
		 
	    
	      String name = ProcessorName.format(projectId, location, processorId);

	      // Read the file.
	      byte[] imageFileData = Files.readAllBytes(Paths.get(filePath));

	      // Convert the image data to a Buffer and base64 encode it.
	      ByteString content = ByteString.copyFrom(imageFileData);

	      RawDocument document =
	          RawDocument.newBuilder().setContent(content).setMimeType("application/pdf").build();

	      // Configure the process request.
	      BatchDocumentsInputConfig documentsConfig = BatchDocumentsInputConfig.newBuilder().setGcsDocuments(null);
		BatchOutputConfig outputConfig;
		BatchProcessRequest request = BatchProcessRequest.newBuilder().setName(name).setInputDocuments(documentsConfig).setOutputConfig(outputConfig).build();


	      BatchProcessRequest.BatchInputConfig batchInputConfig =
	              BatchProcessRequest.BatchInputConfig.newBuilder()
	              .se
	                  .setGcsSource(gcsInputUri)
	                  .setMimeType("application/pdf")
	                  .build();

	      ProcessRequest request =
	          ProcessRequest.newBuilder().setName(name).setRawDocument(document).build();
	      // Recognizes text entities in the PDF document
	      ProcessResponse result = client.processDocument(request);
	      Document documentResponse = result.getDocument();

	      // Get all of the document text as one big string
	      this.text = documentResponse.getText();
	      this.NoPages = documentResponse.getPagesCount();



	      // Read the text recognition output from the processor
	      List<Page> pages = documentResponse.getPagesList();
	      List<String> outImages = new ArrayList<String>();
	      tables = new ArrayList<GoogleTable>();
	      this.mostconfidentlanguage = new ArrayList<String>();
	      for (Iterator<Page> iterator = pages.iterator(); iterator.hasNext();) {
	    	  Page page = (Page) iterator.next();
	    	  List<Table> pagetables = page.getTablesList();
	    	  
	    	  
	    	 for (Iterator iterator2 = pagetables.iterator(); iterator2.hasNext();) {
	    		 Table table = (Table) iterator2.next();
	    		 if (hasHeaders(table, text)) {
	    			 GoogleTable gTable = new GoogleTable();
	    			 gTable.columnNames = new String[table.getHeaderRows(0).getCellsCount()];

	    			 List<TableRow> headers = table.getHeaderRowsList();
	    			 for (int i = 0; i < headers.size(); i++) {
	    				 TableRow tableRow = headers.get(i);
	    				 List<TableCell> rowcells = tableRow.getCellsList();
	    				 for (int j = 0; j < rowcells.size(); j++) {
	    					 gTable.columnNames[j] = (gTable.columnNames[j] == null) ? "" : gTable.columnNames[j];
	    					 TableCell tableCell = rowcells.get(j);
	    					 gTable.columnNames[j] = gTable.columnNames[j] + " "+ getText(tableCell.getLayout(), text);
	    				 }
	    			 }
			
	    			 List<TableRow> rows = table.getBodyRowsList();
	    			 int startIndex =0;
	    			 if (includeHeader) {
	        			 gTable.data = new String[rows.size()+1][maxColumns(table)];
	    				 startIndex =1;
	    				 gTable.data[0] =  gTable.columnNames ;
	    			 }
	    			 else
	    			 {
	        			 gTable.data = new String[rows.size()][maxColumns(table)];
	    				 startIndex =0;
	    			 }
	    			 for (int i = 0; i < rows.size(); i++) {
	    				 TableRow tableRow = rows.get(i);
	    				 List<TableCell> rowcells = tableRow.getCellsList();
	    				 for (int j = 0; j < rowcells.size(); j++) {
	    					 TableCell tableCell = rowcells.get(j);
	    					  gTable.data[i+startIndex][j] = getText(tableCell.getLayout(), text);

	    				 }
	    			 }
	    			 this.tables.add(gTable);
	    		 }
	    	 }



	    	  
	    	  List<DetectedLanguage> detectedlanguages = page.getDetectedLanguagesList();
	    	  String language = "";
	    	  float confidence = 0.0f;
	    	  for (Iterator iterator2 = detectedlanguages.iterator(); iterator2.hasNext();) {
				DetectedLanguage detectedLanguage = (DetectedLanguage) iterator2.next();
				if (detectedLanguage.getConfidence() > confidence) {
					confidence = detectedLanguage.getConfidence();
					language = detectedLanguage.getLanguageCode();
				}
			  }
	    	  mostconfidentlanguage.add(detectedlanguages.get(0).getLanguageCode());

	    	  
	    	  
	    	  if (outputPath != null) {
	    		  outImages.add(drawBoxes(page,filePath,outputPath));
	    	  }


	    	  for (Document.Page.FormField field : page.getFormFieldsList()) {
	    		  String fieldName = getText(field.getFieldName(), text);
	    		  String fieldValue = getText(field.getFieldValue(), text);
	    		  fieldsMap.put(fieldName, fieldValue);
	    	  }
	     }
	      
	      String[] outImagesArray =  new String[outImages.size()];
	      outImagesArray= outImages.toArray(outImagesArray);
	      if (outputPath != null) {
	    	this.OutputFile = outputPath+"/"+getFileName(filePath)+"_annotation.pdf";
			PDFUtils.combineImagesIntoPDF(this.OutputFile, outImagesArray); 
	      }
*/
	    return fieldsMap;
	    
	  }




public  void processDocumentwithSplitterSync (
      String projectId, String location, String processorId, String filePath,String jsonPath)
      throws Exception {

	  LinkedHashMap<String,String> fieldsMap = new LinkedHashMap<String,String>();
	  
	  DocumentProcessorServiceClient client;
	  if (jsonPath != null) {
			 Collection<String> scopes = new ArrayList<String>();
			 scopes.add("https://www.googleapis.com/auth/cloud-platform");
		  GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(jsonPath)).createScoped(scopes);
	      DocumentProcessorServiceSettings settings = DocumentProcessorServiceSettings.newBuilder()
	    		    .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
	    		    .build();
	      client = DocumentProcessorServiceClient.create(settings);
	  }
	  else {
		  
	      client = DocumentProcessorServiceClient.create();
	  }
	  
	  

    
      String name = ProcessorName.format(projectId, location, processorId);

      // Read the file.
      byte[] imageFileData = Files.readAllBytes(Paths.get(filePath));

      // Convert the image data to a Buffer and base64 encode it.
      ByteString content = ByteString.copyFrom(imageFileData);

      Optional<String> extension = getExtensionByStringHandling(filePath);
      
      RawDocument document;
      String mimetype ;
   	  String ext = extension.get().toLowerCase();
      if (ext != "")
      {
    	  switch(ext) {
    	  	case "tiff":  
    	  	case "tif":
    	  			mimetype ="image/tiff";
    	  			break;
    	  	case "gif":
	  				mimetype ="image/tif";
	  				break;
    	  	case "pdf":
	  				mimetype ="application/pdf";
	  				break;
	  		default:
	      	  throw new Exception("File Type "+ext+" not supported");
    	  }
    	  document = RawDocument.newBuilder().setContent(content).setMimeType(mimetype).build();
      }
      else {
    	  throw new Exception("File Type "+ext+" not supported");
      }
    	  


	// Configure the process request.
      ProcessRequest request =
          ProcessRequest.newBuilder().setName(name).setRawDocument(document).build();
      // Recognizes text entities in the PDF document
      ProcessResponse result = client.processDocument(request);
      Document documentResponse = result.getDocument();


      // Get all of the document text as one big string
      this.text = documentResponse.getText();
      
      JSONObject entityObject = new JSONObject();
      JSONArray  entityArray = new JSONArray();
      List<Entity> entitiesResponse = documentResponse.getEntitiesList();

      int i = 1;
      for (Iterator iterator3 = entitiesResponse.iterator(); iterator3.hasNext();) {
  	  	Entity entity = (Entity) iterator3.next();
        JSONObject entityEntry = new JSONObject();
  	  	getEntityProps(entity,entityEntry,true);
  	  	entityArray.put(entityEntry);
      }
      entityObject.put("entities",entityArray );
      entities = entityObject.toString();

    	  
   	}
      

  
  
  
}