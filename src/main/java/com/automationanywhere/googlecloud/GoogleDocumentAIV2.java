
package com.automationanywhere.googlecloud;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.documentai.v1beta2.AutoMlParams;
import com.google.cloud.documentai.v1beta2.Document;
import com.google.cloud.documentai.v1beta2.Document.Page;
import com.google.cloud.documentai.v1beta2.DocumentUnderstandingServiceClient;
import com.google.cloud.documentai.v1beta2.DocumentUnderstandingServiceSettings;
import com.google.cloud.documentai.v1beta2.FormExtractionParams;
import com.google.cloud.documentai.v1beta2.FormExtractionParams.Builder;
import com.google.cloud.documentai.v1beta3.ProcessorName;
import com.google.cloud.documentai.v1beta2.NormalizedVertex;
import com.google.cloud.documentai.v1beta2.Document.TextAnchor;
import com.google.cloud.documentai.v1beta2.Document.Page.Block;
import com.google.cloud.documentai.v1beta2.Document.Page.FormField;
import com.google.cloud.documentai.v1beta2.Document.Page.Layout;
import com.google.cloud.documentai.v1beta2.Document.Entity;
import com.google.cloud.documentai.v1beta2.Document.EntityRelation;
import com.google.cloud.documentai.v1beta2.Document.Page.DetectedLanguage;
import com.google.cloud.documentai.v1beta2.Document.Page.Table;
import com.google.cloud.documentai.v1beta2.Document.Page.Table.TableCell;
import com.google.cloud.documentai.v1beta2.Document.Page.Table.TableRow;
import com.google.cloud.documentai.v1beta2.InputConfig;
import com.google.cloud.documentai.v1beta2.KeyValuePairHint;
import com.google.cloud.documentai.v1beta2.ProcessDocumentRequest;
import com.google.protobuf.ByteString;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONObject;

public class GoogleDocumentAIV2{
	
	
	private Integer NoPages;
	private String OutputFile;

	private List<String> mostconfidentlanguage;
	private List<GoogleTable> tables;
	private String entities;
	private String text;
	



public LinkedHashMap<String,String> processDocumentwithAutoMLSync(String projectId, String location, String autoMlModel, String filePath) throws Exception {

		 DocumentUnderstandingServiceClient client = DocumentUnderstandingServiceClient.create();
		 
		 LinkedHashMap<String,String> fieldsMap = new LinkedHashMap<String,String>();
;

    AutoMlParams params = AutoMlParams.newBuilder().setModel(autoMlModel).build();


    // Read the file.
    byte[] imageFileData = Files.readAllBytes(Paths.get(filePath));

    // Convert the image data to a Buffer and base64 encode it.
    ByteString content = ByteString.copyFrom(imageFileData);

    Optional<String> extension = getExtensionByStringHandling(filePath);
    
    String mimetype ;
 	  String ext = extension.get().toLowerCase();
    if (ext != "")
    {
  	  switch(ext) {
  	  	case "tiff":  
  	  	case "tif":
  	  			mimetype ="image/tiff";
  	  			break;
  	  	case "gif":
	  				mimetype ="image/tif";
	  				break;
  	  	case "pdf":
	  				mimetype ="application/pdf";
	  				break;
	  		default:
	      	  throw new Exception("File Type "+ext+" not supported");
  	  }
    }
    else {
  	  throw new Exception("File Type "+ext+" not supported");
    }
  	
	 String parent = String.format("projects/%s/locations/%s", projectId, location);

    InputConfig config = InputConfig.newBuilder().setContents(content).setMimeType(mimetype).build();
    ProcessDocumentRequest request =ProcessDocumentRequest.newBuilder()
   		 						.setParent(parent)
   		 						.setAutomlParams(params)
   		 						.setInputConfig(config)
   		 						.build();


    Document response = client.processDocument(request);

    this.text = response.getText();
    this.NoPages = response.getPagesCount();


    // Read the text recognition output from the processor
    List<Document.Page> pages = response.getPagesList();
    List<String> outImages = new ArrayList<String>();
 
    this.mostconfidentlanguage = new ArrayList<String>();
    for (Iterator<Document.Page> iterator = pages.iterator(); iterator.hasNext();) {
   	 Document.Page page = (Document.Page) iterator.next();
  	  	List<Document.Page.Table> pagetables = page.getTablesList();
  	  
  	  
  	

  	 JSONObject entityObject = new JSONObject();
        List<Document.Entity> entitiesResponse = response.getEntitiesList();
        List<Document.EntityRelation> entitiesRelations = response.getEntityRelationsList();
        for (Iterator iterator2 = entitiesRelations.iterator(); iterator2.hasNext();) {
        EntityRelation entityRelation = (EntityRelation) iterator2.next();
			 System.out.println(entityRelation.getRelation()+ " Relation" );
		 }
       
      
   for (Iterator iterator3 = entitiesResponse.iterator(); iterator3.hasNext();) {
     	Entity entity = (Entity) iterator3.next();
  	    getEntityProps(entity,entityObject);	
  }
  entities = entityObject.toString();

  	  
 List<DetectedLanguage> detectedlanguages = page.getDetectedLanguagesList();
  	  String language = "";
  	  float confidence = 0.0f;
  	  for (Iterator iterator2 = detectedlanguages.iterator(); iterator2.hasNext();) {
			DetectedLanguage detectedLanguage = (DetectedLanguage) iterator2.next();
			if (detectedLanguage.getConfidence() > confidence) {
				confidence = detectedLanguage.getConfidence();
				language = detectedLanguage.getLanguageCode();
			}
		  }
  	  mostconfidentlanguage.add(detectedlanguages.get(0).getLanguageCode());

  	  
	for ( Document.Page.FormField field : page.getFormFieldsList()) {
  		  String fieldName = getText(field.getFieldName(), text);
  		  String fieldValue = getText(field.getFieldValue(), text);
  		  fieldsMap.put(fieldName, fieldValue);
  	  }
   }
    
    String[] outImagesArray =  new String[outImages.size()];
    outImagesArray= outImages.toArray(outImagesArray);

  return fieldsMap;
}







private static void drawPoly(Graphics2D g, List<NormalizedVertex> vertices, Color color, String label,int w,int h,boolean fill) {
  int[] x = new int[vertices.size()];
  int[] y = new int[vertices.size()];
  for (int i = 0; i < vertices.size(); i++) {
	x[i] = (int)(vertices.get(i).getX()*w);
	y[i] = (int)(vertices.get(i).getY()*h);		
  }
  if (fill) {
	  g.setComposite(AlphaComposite.SrcOver.derive(0.2f));
      g.setColor(color);
	  g.fillPolygon(x, y,vertices.size());
  }
  else
  {   
	  g.setComposite(AlphaComposite.SrcOver.derive(0.6f));
	  g.setColor(color);
      g.drawPolygon(x, y,vertices.size());
	     if (vertices.size() > 1 && label != "") {
 	   g.drawString(label, x[vertices.size()-2], y[vertices.size()-2]);
	     }
  }

}


private static void getEntityProps(Entity entity,JSONObject entityObject) {
	  	System.out.println(entity.getId()+ " "+entity.getType()+" "+entity.getMentionText());

/*	  	if (entity.getPropertiesCount() > 0) {
	  		List<Entity> props = entity.getPropertiesList();
	  		JSONArray propsArray = new JSONArray();
	  		JSONObject subEntityObject = new JSONObject();
	  		for (Iterator iterator = props.iterator(); iterator.hasNext();) {
	  			Entity entityProp = (Entity) iterator.next();
	  			getEntityProps(entityProp,subEntityObject);
	  	
	  		}
  		if (entityObject.has(entity.getType()))
  		{
  			JSONArray currentArray = entityObject.getJSONArray(entity.getType());
  			currentArray.put(subEntityObject);
   	  	    entityObject.put(entity.getType(), currentArray);
  		}
  		else
  		{
	  		propsArray.put(subEntityObject);
   	  	    entityObject.put(entity.getType(), propsArray);
  		}
	  	}
	  	else {
	  	
	  	*/
	  		String[] splitDescription = entity.getType().split("/");
	  		String entityName = (splitDescription.length > 1) ? splitDescription[1] : entity.getType();
	 	  	entityObject.put(entityName , entity.getMentionText());
	//  	}
	  		
	  	
}



private static String getText(Layout layout, String text) {
if (layout.hasTextAnchor()) {
  TextAnchor textAnchor = layout.getTextAnchor();
  if (textAnchor.getTextSegmentsList().size() > 0) {
    int startIdx = (int) textAnchor.getTextSegments(0).getStartIndex();
    int endIdx = (int) textAnchor.getTextSegments(0).getEndIndex();
    return text.substring(startIdx, endIdx);
  }
  return "";
}
else {
	return "";
}

}




private static boolean hasHeaders(Table table, String text) {
  boolean hasHeaderswithContent = false;
  
  List<TableRow> headers = table.getHeaderRowsList();
  for (Iterator iterator3 = headers.iterator(); iterator3.hasNext();) {
	 TableRow tableRow = (TableRow) iterator3.next();
	  List<TableCell> rowcells = tableRow.getCellsList();
		for (Iterator iterator4 = rowcells.iterator(); iterator4.hasNext();) {
			TableCell tableCell = (TableCell) iterator4.next();
			String cellText = getText(tableCell.getLayout(), text);
			hasHeaderswithContent = (cellText =="") ? false : true;
		}
	}
  
  return hasHeaderswithContent;
}

private static int maxColumns(Table table) {
  int maxcolumns = 0;
  
  List<TableRow> rows = table.getBodyRowsList();
  for (int i = 0; i < rows.size(); i++) {
	 TableRow tableRow = rows.get(i);
	 int cellCount = tableRow.getCellsCount();
	 maxcolumns = (cellCount > maxcolumns) ? cellCount : maxcolumns;
 }
  
  return maxcolumns;
}

private static String getFileName(String fullPath) {
		Path path = Paths.get(fullPath); 
      String fileName = path.getFileName().toString();
      int index = fileName.lastIndexOf('.');
      if (index > 0) {
        fileName = fileName.substring(0, index);
      }
    
      return fileName;  
}

public List<String> getLanguage() {
  return this.mostconfidentlanguage;
}
public Integer getpageCount() {
  return this.NoPages;
}

public String getOutputFile() {
  return this.OutputFile;
}


public List<GoogleTable> getTables() {
  return tables;
}


public String getEntities() {
  return entities;
}

public Optional<String> getExtensionByStringHandling(String filename) {
    return Optional.ofNullable(filename)
      .filter(f -> f.contains("."))
      .map(f -> f.substring(filename.lastIndexOf(".") + 1));
}



}


