package com.automationanywhere.botcommand.google.documentai;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class StartDocumentAISessionCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(StartDocumentAISessionCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    StartDocumentAISession command = new StartDocumentAISession();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("authtype") && parameters.get("authtype") != null && parameters.get("authtype").get() != null) {
      convertedParameters.put("authtype", parameters.get("authtype").get());
      if(convertedParameters.get("authtype") !=null && !(convertedParameters.get("authtype") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","authtype", "String", parameters.get("authtype").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("authtype") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","authtype"));
    }
    if(convertedParameters.get("authtype") != null) {
      switch((String)convertedParameters.get("authtype")) {
        case "FILE" : {
          if(parameters.containsKey("jsonPath") && parameters.get("jsonPath") != null && parameters.get("jsonPath").get() != null) {
            convertedParameters.put("jsonPath", parameters.get("jsonPath").get());
            if(convertedParameters.get("jsonPath") !=null && !(convertedParameters.get("jsonPath") instanceof String)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","jsonPath", "String", parameters.get("jsonPath").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("jsonPath") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","jsonPath"));
          }

          if(parameters.containsKey("projectID") && parameters.get("projectID") != null && parameters.get("projectID").get() != null) {
            convertedParameters.put("projectID", parameters.get("projectID").get());
            if(convertedParameters.get("projectID") !=null && !(convertedParameters.get("projectID") instanceof SecureString)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","projectID", "SecureString", parameters.get("projectID").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("projectID") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","projectID"));
          }


        } break;
        case "PARA" : {
          if(parameters.containsKey("authprojectID") && parameters.get("authprojectID") != null && parameters.get("authprojectID").get() != null) {
            convertedParameters.put("authprojectID", parameters.get("authprojectID").get());
            if(convertedParameters.get("authprojectID") !=null && !(convertedParameters.get("authprojectID") instanceof SecureString)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","authprojectID", "SecureString", parameters.get("authprojectID").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("authprojectID") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","authprojectID"));
          }

          if(parameters.containsKey("resourceprojectID") && parameters.get("resourceprojectID") != null && parameters.get("resourceprojectID").get() != null) {
            convertedParameters.put("resourceprojectID", parameters.get("resourceprojectID").get());
            if(convertedParameters.get("resourceprojectID") !=null && !(convertedParameters.get("resourceprojectID") instanceof SecureString)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","resourceprojectID", "SecureString", parameters.get("resourceprojectID").get().getClass().getSimpleName()));
            }
          }

          if(parameters.containsKey("private_key_id") && parameters.get("private_key_id") != null && parameters.get("private_key_id").get() != null) {
            convertedParameters.put("private_key_id", parameters.get("private_key_id").get());
            if(convertedParameters.get("private_key_id") !=null && !(convertedParameters.get("private_key_id") instanceof SecureString)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","private_key_id", "SecureString", parameters.get("private_key_id").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("private_key_id") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","private_key_id"));
          }

          if(parameters.containsKey("private_key") && parameters.get("private_key") != null && parameters.get("private_key").get() != null) {
            convertedParameters.put("private_key", parameters.get("private_key").get());
            if(convertedParameters.get("private_key") !=null && !(convertedParameters.get("private_key") instanceof SecureString)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","private_key", "SecureString", parameters.get("private_key").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("private_key") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","private_key"));
          }

          if(parameters.containsKey("client_email") && parameters.get("client_email") != null && parameters.get("client_email").get() != null) {
            convertedParameters.put("client_email", parameters.get("client_email").get());
            if(convertedParameters.get("client_email") !=null && !(convertedParameters.get("client_email") instanceof SecureString)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","client_email", "SecureString", parameters.get("client_email").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("client_email") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","client_email"));
          }

          if(parameters.containsKey("client_id") && parameters.get("client_id") != null && parameters.get("client_id").get() != null) {
            convertedParameters.put("client_id", parameters.get("client_id").get());
            if(convertedParameters.get("client_id") !=null && !(convertedParameters.get("client_id") instanceof SecureString)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","client_id", "SecureString", parameters.get("client_id").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("client_id") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","client_id"));
          }


        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","authtype"));
      }
    }

    if(parameters.containsKey("location") && parameters.get("location") != null && parameters.get("location").get() != null) {
      convertedParameters.put("location", parameters.get("location").get());
      if(convertedParameters.get("location") !=null && !(convertedParameters.get("location") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","location", "String", parameters.get("location").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("location") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","location"));
    }
    if(convertedParameters.get("location") != null) {
      switch((String)convertedParameters.get("location")) {
        case "us" : {

        } break;
        case "eu" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","location"));
      }
    }

    command.setSessions(sessionMap);
    try {
      command.start((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("authtype"),(String)convertedParameters.get("jsonPath"),(SecureString)convertedParameters.get("projectID"),(SecureString)convertedParameters.get("authprojectID"),(SecureString)convertedParameters.get("resourceprojectID"),(SecureString)convertedParameters.get("private_key_id"),(SecureString)convertedParameters.get("private_key"),(SecureString)convertedParameters.get("client_email"),(SecureString)convertedParameters.get("client_id"),(String)convertedParameters.get("location"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","start"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
